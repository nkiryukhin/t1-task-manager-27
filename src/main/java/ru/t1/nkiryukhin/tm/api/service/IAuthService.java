package ru.t1.nkiryukhin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;
import ru.t1.nkiryukhin.tm.model.User;

public interface IAuthService {

    void checkRoles(@Nullable Role[] roles) throws AbstractException;

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email) throws AbstractException;

    void login(@Nullable String login, @Nullable String password) throws AbstractException;

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId() throws AccessDeniedException;

    @NotNull
    User getUser() throws AbstractException;

}