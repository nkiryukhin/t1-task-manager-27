package ru.t1.nkiryukhin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.api.model.IWBS;
import ru.t1.nkiryukhin.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractUserOwnedModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    private Date created = new Date();

    @NotNull
    private String description = "";

    @NotNull
    private String name = "";

    @NotNull
    private Status status = Status.NOT_STARTED;


    public Project(@NotNull String name) {
        this.name = name;
    }

    public Project(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    public Project(@NotNull String name, @NotNull String description, @NotNull Status status) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}